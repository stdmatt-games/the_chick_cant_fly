source /usr/local/src/stdmatt/shellscript_utils/main.sh

##
##
CURR_OS="$(pw_os_get_simple_name $PW_WSL_AS_GNU_LINUX)";

##
##
export GBDKDIR=/opt/gbdk/
LCC=${GBDKDIR}/bin/lcc;

##
##
$LCC -Igame -Ilibs/pw_gb -Wa-l -Wl-m -Wl-j -c -o build/main_${CURR_OS}.o  game/main.c
$LCC -Igame -Ilibs/pw_gb -Wa-l -Wl-m -Wl-j    -o build/main_${CURR_OS}.gb build/main_${CURR_OS}.o


$LCC -v
