#ifndef __PLATFORM_H__
#define __PLATFORM_H__

#include "game_defs.h"


//----------------------------------------------------------------------------//
// Constants                                                                  //
//----------------------------------------------------------------------------//
#define PLATFORM_SIZE_IN_PX TILE_SIZE_x3
#define PLATFORMS_COUNT     3


//----------------------------------------------------------------------------//
// Types                                                                      //
//----------------------------------------------------------------------------//
typedef struct Platform_Tag {
    U8 x;
    U8 y;

    U8 hits;
    U8 max_hits;

    BOOL shake;
} Platform_t;


//----------------------------------------------------------------------------//
// Globals                                                                    //
//----------------------------------------------------------------------------//
extern Platform_t Platforms[PLATFORMS_COUNT];
extern I8 PlatformSpeedX;

#endif //__PLATFORM_H__
