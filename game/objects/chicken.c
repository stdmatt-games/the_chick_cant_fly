// Header
#include "chicken.h"
// Game
#include "objects/platform.h"
#include "objects/hud.h"



//----------------------------------------------------------------------------//
// Constants                                                                  //
//----------------------------------------------------------------------------//
#define CHICKEN_GRAVITY_ALIVE         1
#define CHICKEN_GRAVITY_DEATH         1
#define CHICKEN_SUPER_GRAVITY         10


#define CHICKEN_IMPULSE               8
#define CHICKEN_DOUBLE_JUMP_IMPUSE    4
#define CHICKEN_SUPER_GRAVITY_IMPULSE 10
#define CHICKEN_MAX_IMPULSE           12


#define CHICKEN_SLOT_FIRST   0
#define CHICKEN_SLOT_END     3
#define CHICKEN_SLOT_COUNT   4

#define CHICKEN_STATE_JUMP  1
#define CHICKEN_STATE_FALL  2
#define CHICKEN_STATE_DYING 3
#define CHICKEN_STATE_DEAD  4


//----------------------------------------------------------------------------//
// Helper Functions                                                           //
//----------------------------------------------------------------------------//
I8
_Chicken_CheckCollision()
{
    U8 index;
    U8 px;
    U8 py;

    for(index = 0; index < PLATFORMS_COUNT; ++index) {
        px = Platforms[index].x;
        py = Platforms[index].y;

        if(CHICKEN_X + CHICKEN_WIDTH_IN_PX < px) {
            continue;
        } else if(CHICKEN_X > px + PLATFORM_SIZE_IN_PX) {
            continue;
        } else if(Chicken.y > py) {
            continue;
        }

        return index;
    }

    return -1;
}

//------------------------------------------------------------------------------
void
_Chicken_SetSprites()
{
    move_sprite(CHICKEN_SLOT_FIRST,     CHICKEN_X            , Chicken.y            );
    move_sprite(CHICKEN_SLOT_FIRST + 1, CHICKEN_X + TILE_SIZE, Chicken.y            );
    move_sprite(CHICKEN_SLOT_FIRST + 2, CHICKEN_X            , Chicken.y + TILE_SIZE);
    move_sprite(CHICKEN_SLOT_FIRST + 3, CHICKEN_X + TILE_SIZE, Chicken.y + TILE_SIZE);

    set_sprite_tile(CHICKEN_SLOT_FIRST,     8 + 0 + Chicken.animation_frame * 4);
    set_sprite_tile(CHICKEN_SLOT_FIRST + 1, 8 + 2 + Chicken.animation_frame * 4);
    set_sprite_tile(CHICKEN_SLOT_FIRST + 2, 8 + 1 + Chicken.animation_frame * 4);
    set_sprite_tile(CHICKEN_SLOT_FIRST + 3, 8 + 3 + Chicken.animation_frame * 4);
}


//----------------------------------------------------------------------------//
// Public Functions                                                           //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
void
Chicken_Reset()
{
    Chicken.y     = Platforms[0].y - CHICKEN_HEIGHT_IN_PX;
    Chicken.max_y = CHICKEN_DEATH_Y;

    Chicken.animation_frame = 0;
    Chicken.jump_momentum   = 0;
    Chicken.gravity         = CHICKEN_GRAVITY_ALIVE;
    Chicken.state           = CHICKEN_STATE_JUMP;

    Chicken.number_of_double_jumps = 1;
    Chicken.score                  = 0;

    _Chicken_SetSprites();
}
U8 GX;

//------------------------------------------------------------------------------
void
Chicken_Update()
{
    I8 platform_collision_index;
    U8 gravity;

    platform_collision_index = -1;
    gravity                  = Chicken.gravity;
    Chicken.max_y            = CHICKEN_DEATH_Y;

    // if(GX > 160) {
    //     GX = 8;
	//     box(0, 0, 160,  140, M_FILL);
    // }

    if(Chicken.state == CHICKEN_STATE_FALL || Chicken.state == CHICKEN_STATE_JUMP)  {
        // Double Jump...
        if(DoubleJump && Chicken.number_of_double_jumps != 0) {
            // Remove Double Jumps.
            Chicken.number_of_double_jumps--;
            Hud_RemoveDoubleJump();

            // Add impulse.
            if(Chicken.jump_momentum < 0) {
                Chicken.jump_momentum = CHICKEN_DOUBLE_JUMP_IMPUSE;
            } else {
                Chicken.jump_momentum += CHICKEN_DOUBLE_JUMP_IMPUSE;
            }

            // Make it as jumping.
            Chicken.state = CHICKEN_STATE_JUMP;

            // gprintf("Y%d       " , (Chicken.jump_momentum));
        }
        // Super Gravity...
        else if(Chicken.state == CHICKEN_STATE_FALL && SuperGravity) {
            gravity = CHICKEN_SUPER_GRAVITY;
        }

        //
        // Collision...
        platform_collision_index = _Chicken_CheckCollision();
        if(platform_collision_index != -1) {
            Chicken.max_y = Platforms[platform_collision_index].y - CHICKEN_HEIGHT_IN_PX;
        }
    }

    //
    // Position.
    if(Chicken.jump_momentum >= CHICKEN_MAX_IMPULSE) {
        Chicken.jump_momentum = CHICKEN_MAX_IMPULSE;
    }
    Chicken.y             = Chicken.y - Chicken.jump_momentum;
    Chicken.jump_momentum = Chicken.jump_momentum - gravity;

    // gotogxy(0, 0);
	// plot(GX, Chicken.jump_momentum, DKGREY, SOLID);
	// plot(GX, Chicken.y+40, BLACK,  SOLID);
	// plot(GX, 100 -(Chicken.jump_momentum + 13), BLACK,  SOLID);
    ++GX;
    if(Chicken.jump_momentum < 0 && Chicken.state == CHICKEN_STATE_JUMP) {
        Chicken.state = CHICKEN_STATE_FALL;
    }

    //printf("%d %d\n", Chicken.y, Chicken.jump_momentum);

    // Hit the top of the screen
    //   Make it snap a little bit higher and start the jump animation
    if(Chicken.y <= CHICKEN_HALF_HEIGHT_IN_PX || Chicken.y >= 0xF0) {
        Chicken.y     = CHICKEN_HALF_HEIGHT_IN_PX;
        Chicken.state = CHICKEN_STATE_FALL;
    }
    //
    // Hit the target position (Death or Platform)
    else if(Chicken.y >= Chicken.max_y) {
        Chicken.y             = Chicken.max_y;   // Snap at position.
        Chicken.jump_momentum = CHICKEN_IMPULSE; // Add the jump force.

        // Hit the platform.
        if(platform_collision_index != -1) {
            Chicken.state   = CHICKEN_STATE_JUMP;
            Chicken.gravity = CHICKEN_GRAVITY_ALIVE;

            ++Platforms[platform_collision_index].hits; // Inform the Platform that it was hit.
            if(SuperGravity) {
                // @XXXX
                // Shake_Reset();
                Chicken.jump_momentum = CHICKEN_SUPER_GRAVITY_IMPULSE;
                Platforms[platform_collision_index].shake |= SuperGravity;
            }
        }

        // Hit the pit - Die!
        else if(Chicken.state == CHICKEN_STATE_FALL) {
            Chicken.state   = CHICKEN_STATE_DYING;
            Chicken.gravity = CHICKEN_GRAVITY_DEATH;
        }

        // Hit the pit as a DEAD chicken.
        else {
            Chicken.state         = CHICKEN_STATE_DEAD;
            Chicken.gravity       = 0;
            Chicken.jump_momentum = 0;
        }
    }


    //
    // Animation Frames.
    if(Chicken.state == CHICKEN_STATE_FALL) {
        Chicken.animation_frame = (Chicken.animation_frame + 1) % 2;
    } else {
        Chicken.animation_frame = 0;
    }

    //
    // Sprites.
    _Chicken_SetSprites();
}



//------------------------------------------------------------------------------
void
Chicken_IncrementScore()
{
    ++Chicken.score;
    Hud_UpdateScore();

    if(Chicken.number_of_double_jumps < DOUBLE_JUMP_MAX_COUNT &&
        Chicken.score > DOUBLE_JUMP_START_SCORE               &&
        Chicken.score % 2)
    {
        ++Chicken.number_of_double_jumps;
        Hud_AddDoubleJump();
    }
}
