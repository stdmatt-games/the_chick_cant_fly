// Header
#include "objects/hud.h"

// Game
#include "objects/chicken.h"


//----------------------------------------------------------------------------//
// Constants                                                                  //
//----------------------------------------------------------------------------//
#define SCORE_SLOT_FIRST         17
#define SCORE_SPRITE_INDEX_FIRST 42

#define DOUBLE_JUMP_SLOT_FIRST 20
#define DOUBLE_JUMP_SLOT_COUNT  2

#define DOUBLE_JUMP_SPRITE_FIRST 52
#define DOUBLE_JUMP_SPRITE_LAST  53

//----------------------------------------------------------------------------//
// Functions                                                                  //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
void
Hud_UpdateScore()
{
    U8 a, b, c;

    a = Chicken.score % 10;
    b = Chicken.score / 10 % 10;
    c = Chicken.score / 10 / 10;

    set_sprite_tile(SCORE_SLOT_FIRST    , SCORE_SPRITE_INDEX_FIRST + c);
    set_sprite_tile(SCORE_SLOT_FIRST + 1, SCORE_SPRITE_INDEX_FIRST + b);
    set_sprite_tile(SCORE_SLOT_FIRST + 2, SCORE_SPRITE_INDEX_FIRST + a);

    move_sprite(SCORE_SLOT_FIRST,     SCREEN_WIDTH / 2 - TILE_SIZE + 4,  20);
    move_sprite(SCORE_SLOT_FIRST + 1, SCREEN_WIDTH / 2             + 4,  20);
    move_sprite(SCORE_SLOT_FIRST + 2, SCREEN_WIDTH / 2 + TILE_SIZE + 4 , 20);
}


//------------------------------------------------------------------------------
void
Hud_ResetDoubleJump()
{
    U8 index;
    U8 sprite_id;

    for(index = 0; index < DOUBLE_JUMP_MAX_COUNT; ++index) {
        sprite_id = DOUBLE_JUMP_SLOT_FIRST + index * DOUBLE_JUMP_SLOT_COUNT;

        set_sprite_tile(sprite_id,     DOUBLE_JUMP_SPRITE_FIRST);
        set_sprite_tile(sprite_id + 1, DOUBLE_JUMP_SPRITE_LAST);

        if(index < Chicken.number_of_double_jumps) {
            move_sprite(sprite_id    , 140 + index * TILE_SIZE, 18);
            move_sprite(sprite_id + 1, 140 + index * TILE_SIZE, 18 + TILE_SIZE);
        } else {
            move_sprite(sprite_id    , 0, 0);
            move_sprite(sprite_id + 1, 0, 0);
        }
    }
}


//------------------------------------------------------------------------------
void
Hud_AddDoubleJump()
{
    U8 sprite_id;
    U8 index;

    index     = Chicken.number_of_double_jumps - 1;
    sprite_id = DOUBLE_JUMP_SLOT_FIRST + (index * DOUBLE_JUMP_SLOT_COUNT);

    set_sprite_tile(sprite_id,     DOUBLE_JUMP_SPRITE_FIRST);
    set_sprite_tile(sprite_id + 1, DOUBLE_JUMP_SPRITE_LAST);

    move_sprite(sprite_id    , 140 + index * TILE_SIZE, 18);
    move_sprite(sprite_id + 1, 140 + index * TILE_SIZE, 18 + TILE_SIZE);
}


//------------------------------------------------------------------------------
void
Hud_RemoveDoubleJump()
{
    U8 sprite_id;
    sprite_id = DOUBLE_JUMP_SLOT_FIRST + (Chicken.number_of_double_jumps) * DOUBLE_JUMP_SLOT_COUNT;

    move_sprite(sprite_id    , 0, 0);
    move_sprite(sprite_id + 1, 0, 0);
}
