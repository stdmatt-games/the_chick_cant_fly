#ifndef __CHICKEN_H__
#define __CHICKEN_H__

#include "game_defs.h"

//----------------------------------------------------------------------------//
// Constants                                                                  //
//----------------------------------------------------------------------------//
#define CHICKEN_HALF_HEIGHT_IN_PX  TILE_SIZE
#define CHICKEN_HEIGHT_IN_PX       TILE_SIZE_x2
#define CHICKEN_WIDTH_IN_PX        TILE_SIZE_x2


//----------------------------------------------------------------------------//
// Types                                                                      //
//----------------------------------------------------------------------------//
typedef struct Chicken_Tag {
    U8 y;
    U8 max_y;

    U8 animation_frame;
    I8 jump_momentum;
    U8 gravity;

    U8 state;

    U8 number_of_double_jumps;
    U8 score;
} Chicken_t;


//----------------------------------------------------------------------------//
// Functions                                                                  //
//----------------------------------------------------------------------------//
void Chicken_IncrementScore();


//----------------------------------------------------------------------------//
// Globals                                                                    //
//----------------------------------------------------------------------------//
extern Chicken_t Chicken;
extern BOOL      SuperGravity;
extern BOOL      DoubleJump;

extern U8 GX;
#endif // __CHICKEN_H__
