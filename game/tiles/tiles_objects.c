/*

 TILES_OBJECTS.C

 Tile Source File.

 Info:
  Form                 : All tiles as one unit.
  Format               : Gameboy 4 color.
  Compression          : None.
  Counter              : None.
  Tile size            : 8 x 8
  Tiles                : 0 to 53

  Palette colors       : None.
  SGB Palette          : None.
  CGB Palette          : None.

  Convert to metatiles : No.

 This file was generated by GBTD v2.2

*/

/* Start of tile array. */
unsigned char TILES_OBJECTS[] =
{
  0x00,0x00,0x01,0x01,0x00,0x00,0x01,0x01,
  0x7A,0x7B,0xFD,0x86,0xFF,0x80,0x7F,0x40,
  0xFF,0x80,0xBF,0xC0,0x9F,0xE1,0xCF,0x71,
  0xE2,0x3F,0xFE,0x1F,0x7F,0x03,0x3F,0x00,
  0xF0,0xF0,0xF8,0xF8,0xFC,0xFC,0x7C,0x94,
  0xFE,0x02,0xFE,0xEE,0xFF,0x1F,0xFE,0x0E,
  0xFD,0x03,0xFD,0x03,0xF9,0x07,0x63,0x9E,
  0x07,0xFC,0x1F,0xF8,0xFF,0xE0,0xFE,0x00,
  0x00,0x00,0x00,0x00,0x01,0x01,0x00,0x00,
  0x79,0x79,0xFE,0x87,0xFD,0x86,0x7F,0x40,
  0xFF,0x80,0xBF,0xC0,0x9F,0xE1,0xCD,0x73,
  0xE2,0x3F,0xFE,0x1F,0x7F,0x03,0x3F,0x00,
  0x00,0x00,0xF0,0xF0,0xF8,0xF8,0xFC,0xFC,
  0x7C,0x94,0xFE,0x02,0xFE,0xEE,0xFF,0x1F,
  0xFF,0x0F,0xFF,0x01,0xFD,0x03,0xF9,0x07,
  0xC3,0x3E,0x0F,0xFC,0xFF,0xF0,0xFE,0x00,
  0x00,0x00,0x01,0x01,0x00,0x00,0x01,0x01,
  0x7B,0x7A,0xFD,0x86,0xF9,0x86,0x7F,0x40,
  0x7F,0x60,0x7F,0x40,0x7F,0x41,0x7F,0x41,
  0x3F,0x23,0x1E,0x1E,0x01,0x01,0x00,0x00,
  0xF0,0xF0,0xF8,0xF8,0xFC,0xFC,0xFC,0x14,
  0xFE,0x42,0xFE,0x4E,0xFF,0x5F,0xFE,0x0E,
  0xFD,0x03,0xF9,0x07,0xF1,0x0F,0x83,0xFF,
  0x45,0x7D,0xFB,0xFB,0x05,0x05,0x02,0x02,
  0x00,0x00,0x01,0x01,0x78,0x78,0xFD,0x85,
  0x7E,0x43,0xFE,0x83,0xFF,0x81,0x7F,0x40,
  0x7F,0x70,0x4F,0x78,0x27,0x38,0x13,0x1C,
  0x0D,0x0F,0x06,0x06,0x05,0x05,0x02,0x02,
  0xF0,0xF0,0xFE,0xFE,0xFD,0xFF,0x7D,0x97,
  0xFE,0x46,0xFF,0x4F,0xFE,0x5E,0xEF,0x1F,
  0xF7,0x0F,0xF9,0x07,0xF9,0x07,0xF2,0x0E,
  0x84,0xFC,0xFA,0xFA,0x14,0x14,0x08,0x08,
  0xFF,0x7F,0xFF,0x7F,0xE7,0x63,0xFF,0x7F,
  0xFF,0x7F,0xE0,0x60,0xE0,0x60,0xE0,0x60,
  0xFF,0x7F,0xFF,0x7F,0xE7,0x63,0xFF,0x7F,
  0xFC,0x7C,0xEE,0x66,0xE7,0x63,0xE3,0x61,
  0xFF,0x7F,0xFF,0x7F,0xE0,0x60,0xFF,0x7F,
  0xFF,0x7F,0xE0,0x60,0xFF,0x7F,0xFF,0x7F,
  0xFF,0x7F,0xFF,0x7F,0xE0,0x60,0xFF,0x7F,
  0xFF,0x7F,0x07,0x03,0xFF,0x7F,0xFF,0x7F,
  0x3C,0x3C,0x7E,0x42,0xFF,0xBD,0xFF,0xA5,
  0xFF,0xBD,0xFF,0xA5,0x7E,0x42,0x3C,0x3C,
  0x00,0x00,0x00,0x00,0x00,0x00,0xFB,0xFB,
  0x80,0x80,0xF8,0xF8,0x08,0x08,0xF8,0xF8,
  0x00,0x00,0x00,0x00,0x00,0x00,0xEF,0xEF,
  0x88,0x88,0x88,0x88,0x88,0x88,0x8F,0x8F,
  0x00,0x00,0x00,0x00,0x00,0x00,0x36,0x36,
  0xAA,0xAA,0xA2,0xA2,0xA2,0xA2,0x22,0x22,
  0x00,0x00,0x00,0x00,0x00,0x00,0xFB,0xFB,
  0x88,0x88,0x88,0x88,0xF8,0xF8,0x88,0x88,
  0x00,0x00,0x00,0x00,0x00,0x00,0xEF,0xEF,
  0x82,0x82,0x82,0x82,0x82,0x82,0x82,0x82,
  0x00,0x00,0x00,0x00,0x00,0x00,0x87,0x87,
  0x01,0x01,0x37,0x37,0x04,0x04,0x07,0x07,
  0x00,0x00,0x00,0x00,0x00,0x00,0x77,0x77,
  0x51,0x51,0x57,0x57,0x54,0x54,0x77,0x77,
  0x00,0x00,0x00,0x00,0x00,0x00,0x74,0x74,
  0x54,0x54,0x54,0x54,0x50,0x50,0x74,0x74,
  0xFF,0xFF,0xE1,0x9F,0xC1,0xBF,0x81,0xFF,
  0xFF,0xFF,0x90,0xFF,0x90,0xFF,0xFF,0xFF,
  0xFF,0xFF,0x01,0xFF,0x01,0xFF,0x01,0xFF,
  0xFF,0xFF,0x20,0xFF,0x20,0xFF,0xFF,0xFF,
  0xFF,0xFF,0x01,0xFF,0x01,0xFF,0x01,0xFF,
  0xFF,0xFF,0x09,0xFF,0x09,0xFF,0xFF,0xFF,
  0x00,0x00,0x1F,0x1F,0x11,0x11,0x11,0x11,
  0x11,0x11,0x11,0x11,0x11,0x11,0x1F,0x1F,
  0x00,0x00,0x1C,0x1C,0x04,0x04,0x04,0x04,
  0x04,0x04,0x04,0x04,0x04,0x04,0x1F,0x1F,
  0x00,0x00,0x1F,0x1F,0x01,0x01,0x1F,0x1F,
  0x10,0x10,0x10,0x10,0x10,0x10,0x1F,0x1F,
  0x00,0x00,0x1F,0x1F,0x01,0x01,0x1F,0x1F,
  0x01,0x01,0x01,0x01,0x01,0x01,0x1F,0x1F,
  0x00,0x00,0x11,0x11,0x11,0x11,0x1F,0x1F,
  0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
  0x00,0x00,0x1F,0x1F,0x10,0x10,0x1F,0x1F,
  0x01,0x01,0x01,0x01,0x01,0x01,0x1F,0x1F,
  0x00,0x00,0x1F,0x1F,0x10,0x10,0x1F,0x1F,
  0x11,0x11,0x11,0x11,0x11,0x11,0x1F,0x1F,
  0x00,0x00,0x1F,0x1F,0x01,0x01,0x01,0x01,
  0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
  0x00,0x00,0x1F,0x1F,0x11,0x11,0x1F,0x1F,
  0x11,0x11,0x11,0x11,0x11,0x11,0x1F,0x1F,
  0x00,0x00,0x1F,0x1F,0x11,0x11,0x1F,0x1F,
  0x01,0x01,0x01,0x01,0x01,0x01,0x1F,0x1F,
  0x1C,0x1C,0x3E,0x22,0x7F,0x49,0x7F,0x49,
  0x7F,0x49,0x7F,0x41,0x3E,0x22,0x1C,0x1C,
  0x0E,0x0E,0x1E,0x12,0x3E,0x22,0x3E,0x32,
  0x1E,0x12,0x1E,0x12,0x1E,0x12,0x1E,0x1E,
  0x1C,0x1C,0x3E,0x22,0x7F,0x49,0x7F,0x79,
  0x3E,0x22,0x7F,0x4F,0x7F,0x41,0x7F,0x7F,
  0x1C,0x1C,0x3E,0x22,0x7F,0x49,0x7F,0x79,
  0x1E,0x12,0x7F,0x79,0x7E,0x42,0x3C,0x3C,
  0x0E,0x0E,0x1E,0x12,0x3E,0x2A,0x3F,0x2B,
  0x7F,0x41,0x7F,0x7B,0x0E,0x0A,0x0E,0x0E,
  0x7F,0x7F,0x7F,0x41,0x7F,0x4D,0x7E,0x42,
  0x3F,0x39,0x7F,0x49,0x3E,0x22,0x1C,0x1C,
  0x1C,0x1C,0x3E,0x22,0x7F,0x49,0x7F,0x4F,
  0x7F,0x41,0x7F,0x49,0x3E,0x22,0x1C,0x1C,
  0x7F,0x7F,0x7F,0x41,0x7F,0x79,0x0F,0x09,
  0x0F,0x09,0x1E,0x12,0x1E,0x12,0x1E,0x1E,
  0x1C,0x1C,0x3E,0x22,0x7F,0x49,0x3E,0x22,
  0x7F,0x49,0x7F,0x49,0x3E,0x22,0x1C,0x1C,
  0x1C,0x1C,0x3E,0x22,0x7F,0x49,0x3F,0x21,
  0x7F,0x79,0x7F,0x49,0x3E,0x22,0x1C,0x1C,
  0x00,0x00,0x00,0x40,0x10,0x12,0x38,0x28,
  0x3F,0x27,0x7F,0x41,0xFE,0x82,0xFE,0xC2,
  0x3E,0x2A,0x36,0x36,0x20,0xA0,0x00,0x02,
  0x00,0x3C,0x00,0x08,0x00,0x28,0x00,0x38
};

/* End of TILES_OBJECTS.C */
