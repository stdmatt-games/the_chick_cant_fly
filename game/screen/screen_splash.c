// Header
#include "screen_splash.h"
// Game
#include "game_defs.h"


//----------------------------------------------------------------------------//
// Constants                                                                  //
//----------------------------------------------------------------------------//
// Title
#define _SPLASH_TARGET_Y -20

// Press A
#define _P_ 16
#define _R_ 17
#define _E_ 18
#define _S_ 19
#define _A_ 20

#define PRESS_A_X   0x3B
#define PRESS_A_Y   0x75
#define PRESS_A_GAP 0x02

// Copyright
#define COPYRIGHT_SPRITES_COUNT     8
#define COPYRIGHT_SLOT_START        6
#define COPYRIGHT_TILE_INDEX_START 21

#define COPYRIGHT_X 0x39
#define COPYRIGHT_Y 0x96

// Chicken
#define LOGO_CHICKEN_SPRITES_COUNT     4
#define LOGO_CHICKEN_SLOT_START       22
#define LOGO_CHICKEN_TILE_INDEX_START  0

#define LOGO_CHICKEN_X (PRESS_A_X + TILE_SIZE * 2 + PRESS_A_GAP * 2)
#define LOGO_CHICKEN_Y (PRESS_A_Y - TILE_SIZE * 2 - 4)


//----------------------------------------------------------------------------//
// Helper Functions                                                           //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
void
_Splash_SetChickenFrame(U8 index)
{
    index = (index * 4);

    set_sprite_tile(LOGO_CHICKEN_SLOT_START    , LOGO_CHICKEN_TILE_INDEX_START     + index);
    set_sprite_tile(LOGO_CHICKEN_SLOT_START + 1, LOGO_CHICKEN_TILE_INDEX_START + 1 + index);
    set_sprite_tile(LOGO_CHICKEN_SLOT_START + 2, LOGO_CHICKEN_TILE_INDEX_START + 2 + index);
    set_sprite_tile(LOGO_CHICKEN_SLOT_START + 3, LOGO_CHICKEN_TILE_INDEX_START + 3 + index);
}


//----------------------------------------------------------------------------//
// Public Functions                                                           //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
void
Splash_Init()
{
    U8 index;

    VBK_REG = 1;
    HIDE_SPRITES;
    HIDE_BKG;

    //
    // Setup Background.
    set_bkg_data(0, 128, TILES_SPLASH);
    set_bkg_tiles(0, 0, 20, 18, MAP_SPLASH);
    move_bkg(0, 0);

    //
    // Setup the Sprites
    set_sprite_data(0, OBJECTS_TILES_COUNT, TILES_OBJECTS);

    // Remove all sprites from the screen.
    for(index = 0; index < OBJECTS_TILES_COUNT; ++index) {
        move_sprite(index, 0, 0);
    }

    // Press (A).
    set_sprite_tile(0, _P_); // P
    set_sprite_tile(1, _R_); // R
    set_sprite_tile(2, _E_); // E
    set_sprite_tile(3, _S_); // S
    set_sprite_tile(4, _S_); // S
    set_sprite_tile(5, _A_); // (A)

    for(index = 0; index < 6; ++index) {
        move_sprite(index, PRESS_A_X + index * TILE_SIZE + index * PRESS_A_GAP, PRESS_A_Y);
    }

    // Copyright.
    for(index = 0; index < COPYRIGHT_SPRITES_COUNT; ++index) {
        set_sprite_tile(COPYRIGHT_SLOT_START + index,  COPYRIGHT_TILE_INDEX_START + index);
        move_sprite(COPYRIGHT_SLOT_START + index, COPYRIGHT_X + index * TILE_SIZE, COPYRIGHT_Y);
    }

    // Chicken.
    move_sprite(LOGO_CHICKEN_SLOT_START    , LOGO_CHICKEN_X            , LOGO_CHICKEN_Y);
    move_sprite(LOGO_CHICKEN_SLOT_START + 1, LOGO_CHICKEN_X            , LOGO_CHICKEN_Y + TILE_SIZE);
    move_sprite(LOGO_CHICKEN_SLOT_START + 2, LOGO_CHICKEN_X + TILE_SIZE, LOGO_CHICKEN_Y);
    move_sprite(LOGO_CHICKEN_SLOT_START + 3, LOGO_CHICKEN_X + TILE_SIZE, LOGO_CHICKEN_Y + TILE_SIZE);

    SHOW_BKG;
    VBK_REG = 0;
}

//------------------------------------------------------------------------------
void
Splash_Update()
{
    INT16 current_title_y;
    U8    frame_count;
    BOOL  showing_sprites;
    BOOL  exiting;

    current_title_y = 0;
    frame_count     = 0;
    showing_sprites = FALSE;
    exiting         = FALSE;

    while(1) {
        // Show Splash
        if(!exiting) {
            // Animate the Logo.
            if(!showing_sprites && current_title_y > _SPLASH_TARGET_Y)  {
                current_title_y -= 2;
                move_bkg(0, current_title_y);
            }
            // Delay to show sprites
            else if(!showing_sprites) {
                ++frame_count;
                if(frame_count == 2) {
                    showing_sprites = TRUE;
                    frame_count     = 0;

                    _Splash_SetChickenFrame(0);

                    Delay(1);
                    SHOW_SPRITES;
                }
            }
            // Animate the Chicken.
            else {
                _Splash_SetChickenFrame((frame_count & 0x8) == 0);
                ++frame_count;

                if(joypad() & J_A) {
                    exiting = TRUE;

                    Fade_Reset(FADE_FRAMES_COUNT, FADE_OUT);
                    HIDE_SPRITES;
                }
            }
        }
        // Fading out
        else if(!Fade_IsCompleted) {
            Fade_Update();
        }
        // Fade completed
        else {
            break;
        }

        // Refresh...
        Delay(1);
        wait_vbl_done();
    }

    Screen_Change(SCREEN_TYPE_GAME);
}

//------------------------------------------------------------------------------
void
Splash_Quit()
{
    U8 index;

    // Remove all sprites from the screen.
    for(index = 0; index < OBJECTS_TILES_COUNT; ++index) {
        move_sprite(index, 0, 0);
    }

    HIDE_SPRITES;
    HIDE_BKG;
}
