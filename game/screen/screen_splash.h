#ifndef __SCREEN_SPLASH_H__
#define __SCREEN_SPLASH_H__

void Splash_Init  ();
void Splash_Update();
void Splash_Quit  ();

#endif // __SCREEN_SPLASH_H__
