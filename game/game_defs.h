#ifndef __GAME_DEFS_H__
#define __GAME_DEFS_H__

// pw
#include "pw_gb/pw_gb.h"


//----------------------------------------------------------------------------//
// Game Constants                                                             //
//----------------------------------------------------------------------------//
#define SCREEN_TYPE_SPLASH  0
#define SCREEN_TYPE_GAME    2



#define MIN_PLATFORM_GAP_X 40
#define MAX_PLATFORM_GAP_X 65

#define MIN_PLATFORM_GAP_Y 75
#define MAX_PLATFORM_GAP_Y 135

#define CHICKEN_X        50
#define CHICKEN_DEATH_Y 160

#define FADE_FRAMES_COUNT 12

#define DOUBLE_JUMP_START_SCORE 5
#define DOUBLE_JUMP_MAX_COUNT   3


#endif // __GAME_DEFS_H__
